package ru.levelp.at.homework6.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import ru.levelp.at.homework6.EmailPage;

@Epic("Email tests")
public class CreateEmailWithTestTitleTest extends BaseClass {

    @Severity(SeverityLevel.NORMAL)
    @Feature("Create a special title")
    @Issue("MBS-13330")
    @Description("Filtering email by word 'Test' in a title")
    @Test(dataProvider = "test2", description = "Test with valid data")

    public void createEmailWithTestTitle(String email, String title, String body) {
        EmailPage emailPage = new EmailPage(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Написать письмо']")));
        emailPage.refresh();
        emailPageSteps.createNewEmail(email, title, body);
        emailPageSteps.sentEmail();
        emailPageSteps.closeInfoModal();
        emailPage.refresh();
        emailPageSteps.checkSentFolderEmailItester(email);
        Pattern p = Pattern.compile(REGEX_TEST2);
        Matcher m = p.matcher(title);
        if (m.find()) {
            emailPage.refresh();
            emailPageSteps.checkAllFolders();
            emailPageSteps.openSidebar();
            emailPageSteps.checkTestFolder(email);
        }

    }
}
