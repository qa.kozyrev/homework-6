package ru.levelp.at.homework6.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import ru.levelp.at.homework6.EmailPage;

@Epic("Email tests")
public class CreateDraftAndSentTest extends BaseClass {

    @Severity(SeverityLevel.CRITICAL)
    @Feature("Create a draft")
    @Issue("MBS-13331")
    @Description("Creating a draft and sent it")
    @Test(dataProvider = "test1", description = "Test with valid data")

    public void createDraftAndSent(String email, String title, String body) {
        EmailPage emailPage = new EmailPage(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Написать письмо']")));
        emailPage.refresh();
        emailPageSteps.createNewEmail(email, title, body);
        emailPageSteps.saveTheEmail();
        emailPageSteps.closeTheEmailModal();
        emailPageSteps.openDraftsFolder();
        emailPageSteps.checkEmailByTitle(title);
        emailPageSteps.checkEmailBody();
        emailPageSteps.sentEmail();
        emailPageSteps.closeInfoModal();
        emailPageSteps.checkDraftsFolderIsEmpty();
        emailPageSteps.checkSentFolderQaKozyrev(title);
    }
}
