package ru.levelp.at.homework6.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import ru.levelp.at.homework6.EnvironmentResourcesGenerator;
import ru.levelp.at.homework6.steps.EmailPageSteps;
import ru.levelp.at.homework6.steps.LoginRegistrationSteps;

public class BaseClass {

    protected static final String URL = "https://mail.ru";
    protected static WebDriver driver;
    public static Capabilities capabilities;
    public static final String EMAIL = "itester@internet.ru";
    public static final String PASSWORD = "1qazXSW@12345";
    public static final String FOLDER_FOR_TEST2 = "Тест";
    public static final String REGEX_TEST2 = "\\bТест\\b";
    protected EmailPageSteps emailPageSteps;
    protected LoginRegistrationSteps loginRegistrationSteps;

    public static WebDriver getDriver() {
        return driver;
    }

    @DataProvider
    public Object[][] test1() {
        return new Object[][] {{"qa.kozyrev@gmail.com", "Test", "TestBody"}};
    }

    @DataProvider
    public Object[][] test2() {
        return new Object[][] {{"itester@internet.ru", "Тест Заголовок", "TestBody"}};
    }

    @DataProvider
    public Object[][] test3() {
        return new Object[][] {{"itester@internet.ru", "Title", "TestBody"}};
    }

    @BeforeClass
    static void beforeAll() {
        new EnvironmentResourcesGenerator(capabilities).createProperties();
    }

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to("https://mail.ru");
        emailPageSteps = new EmailPageSteps(driver);
        loginRegistrationSteps = new LoginRegistrationSteps(driver);
        loginRegistrationSteps.openPage();
        loginRegistrationSteps.loginUser("itester@internet.ru", "1qazXSW@12345");
        emailPageSteps.assertUserRegistration("itester@internet.ru");
    }

    @AfterClass
    public void tearDown() {
        emailPageSteps.logout();
        driver.quit();
    }

    @AfterClass
    void afterClass() {
        new EnvironmentResourcesGenerator(((RemoteWebDriver) driver).getCapabilities()).createProperties();
    }
}
