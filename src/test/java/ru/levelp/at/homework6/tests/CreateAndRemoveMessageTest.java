package ru.levelp.at.homework6.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import ru.levelp.at.homework6.EmailPage;

@Epic("Email tests")
public class CreateAndRemoveMessageTest extends BaseClass {

    @Severity(SeverityLevel.MINOR)
    @Feature("Remove sent message")
    @Issue("MBS-13332")
    @Description("Creating and removing a message")
    @Test(dataProvider = "test3")
    public void createAndRemoveMessage(String email, String title, String body) {

        EmailPage emailPage = new EmailPage(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/compose/']")));
        emailPage.refresh();
        emailPageSteps.createNewEmail(email, title, body);
        emailPageSteps.sentEmail();
        emailPageSteps.closeInfoModal();
        emailPageSteps.checkSentEmail(title);
        emailPageSteps.removeEmail(title);
        emailPageSteps.openSidebar();
        emailPageSteps.openTrashFolder();
        emailPageSteps.checkSentEmail(title);
    }
}
