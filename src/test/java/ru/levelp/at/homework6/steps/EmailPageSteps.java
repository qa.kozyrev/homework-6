package ru.levelp.at.homework6.steps;

import io.qameta.allure.Step;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.levelp.at.homework6.EmailPage;
import ru.levelp.at.homework6.tests.BaseClass;

public class EmailPageSteps extends BaseClass {

    private final EmailPage emailPage;

    public EmailPageSteps(WebDriver driver) {
        BaseClass.driver = driver;
        emailPage = new EmailPage(driver);
    }

    @Step("Check user registration {expectedEmail}")
    public void assertUserRegistration(String expectedEmail) {
        EmailPage emailPage = new EmailPage(driver);
        emailPage.clickAccountDataButton();
        String actualEmail = emailPage.checkEmailInSidePanel();
        Assertions.assertThat(actualEmail).contains(new CharSequence[] {expectedEmail});
    }

    @Step("Log out")
    public void logout() {
        emailPage.clickAccountDataButton();
        emailPage.clickLogoutButton();
    }

    @Step("Create a new email")
    public void createNewEmail(String email, String title, String body) {
        emailPage.createNewEmail();
        emailPage.fillEmail(email);
        emailPage.fillTittle(title);
        emailPage.fillBody(body);
    }

    @Step("Sent the email")
    public void sentEmail() {
        emailPage.sentEmail();
    }

    @Step("Close the info modal")
    public void closeInfoModal() {
        emailPage.closeInfoModal();
    }

    @Step("Check the email was sent")
    public void checkSentEmail(String title) {
        emailPage.checkSentEmailTitleItester();
        String actualEmail = emailPage.getEmailTitle();
        Assertions.assertThat(actualEmail).isEqualTo(title);
    }

    @Step("Select the email by title and remove it")
    public void removeEmail(String title) {
        emailPage.selectMessageByEmailItester();
        emailPage.clickRemoveButton();
    }

    @Step("Open the sidebar")
    public void openSidebar() {
        emailPage.openSideBar();
    }

    @Step("Open the trash folder")
    public void openTrashFolder() {
        emailPage.openTrashFolder();
    }

    @Step("Save the email")
    public void saveTheEmail() {
        emailPage.saveEmail();
    }

    @Step("Close the email modal")
    public void closeTheEmailModal() {
        emailPage.closeEmailModal();
    }

    @Step("Open the 'Drafts' folder")
    public void openDraftsFolder() {
        emailPage.openDraftsFolder();
    }

    @Step("Check the email by title")
    public void checkEmailByTitle(String title) {
        emailPage.selectMessageByEmailQaKozyrev();
        String actualTitle = emailPage.getTitle();
        Assertions.assertThat(actualTitle).isEqualTo(title);
    }

    @Step("Check email body")
    public void checkEmailBody() {
        emailPage.checkingEmailBody();
    }

    @Step("Check 'Drafts' folder is empty")
    public void checkDraftsFolderIsEmpty() {
        emailPage.checkDraftsFolderIsEmpty();
    }

    @Step("Check 'Sent' folder QaKozyrev")
    public void checkSentFolderQaKozyrev(String title) {
        emailPage.selectSentFolder();
        emailPage.selectMessageByEmailQaKozyrev();
        String actualEmailTitle = emailPage.getTitle();
        Assertions.assertThat(actualEmailTitle).isEqualTo(title);
    }

    @Step("Check the email in 'Sent' folder")
    public void checkSentFolderEmailItester(String email) {
        emailPage.selectSentFolder();
        emailPage.selectMessageByEmailItester();
        String actualEmail = emailPage.getEmailAddress();
        Assertions.assertThat(actualEmail).isEqualTo(EMAIL);
    }

    @Step("Select 'All Folders' folder")
    public void checkAllFolders() {
        emailPage.selectAllFoldersFolder();
    }

    @Step("Check 'Test Folder' folder")
    public void checkTestFolder(String actualEmail) {
        WebElement ele = driver.findElement(By.xpath("//a[@title='Тест, 1 письмо, 1 непрочитанное']"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ele);
        emailPage.checkSentEmailTitleItester();
        Assertions.assertThat(actualEmail).isEqualTo("itester@internet.ru");
    }
}
