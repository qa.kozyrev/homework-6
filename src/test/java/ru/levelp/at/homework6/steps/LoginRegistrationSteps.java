package ru.levelp.at.homework6.steps;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.homework6.LoginPage;
import ru.levelp.at.homework6.tests.BaseClass;

public class LoginRegistrationSteps extends BaseClass {

    private final LoginPage loginPage;

    public LoginRegistrationSteps(WebDriver driver) {
        BaseClass.driver = driver;
        loginPage = new LoginPage(driver);
    }

    @Step("Open the main page")
    public void openPage() {
        loginPage.openEmailModal();
    }

    @Step("Fill registration fields {email}, {password}")
    public void loginUser(String email, String password) {
        loginPage.switchIframe();
        loginPage.enterEmail("itester@internet.ru");
        loginPage.clickNextButtonInEmailModal();
        loginPage.enterPassword("1qazXSW@12345");
        loginPage.clickSubmitButtonInEmailModal();
        driver.switchTo().defaultContent();
    }
}
