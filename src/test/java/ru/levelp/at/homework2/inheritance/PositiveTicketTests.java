package ru.levelp.at.homework2.inheritance;

import java.util.List;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import ru.levelp.at.homework2.YourTicket;

public class PositiveTicketTests extends BaseTicketTests {

    @Test(dataProvider = "positiveYourTicketDataProvider", dataProviderClass = ExternalDataProvider.class)

    public static void positiveYourTicket(String input, boolean expectedOutput) {
        List<Boolean> actualOutput = YourTicket.luckyTicket(input);
        Assertions.assertThat(actualOutput).isSubsetOf(expectedOutput);
    }
}
