package ru.levelp.at.homework2.inheritance;

import org.testng.annotations.DataProvider;

public class ExternalDataProvider {

    @DataProvider
    public static Object[][] positiveYourTicketDataProvider() {
        return new Object[][] {
            {"123321", true},
            {"001011", false},
            {"000000", true},
            {"000001", false},
            {"999999", true},
        };
    }

    @DataProvider
    public static Object[][] negativeYourTicketDataProvider() {
        return new Object[][] {
            {"22100", false},
            {"000010", false},
            {"-000001", false},
            {"11111.1", false},
            {"123V23", false},
            {"", false},
            {null, false},
        };
    }

}

