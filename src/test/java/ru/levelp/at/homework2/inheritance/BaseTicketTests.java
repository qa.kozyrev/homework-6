package ru.levelp.at.homework2.inheritance;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


public abstract class BaseTicketTests {

    @BeforeClass
    public void beforeClass() {
        System.out.println("Tests in selected class are started");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Test is started");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("Test is finished");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("Tests in selected class are finished");
    }
}
