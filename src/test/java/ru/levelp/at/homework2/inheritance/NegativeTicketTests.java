package ru.levelp.at.homework2.inheritance;

import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import ru.levelp.at.homework2.YourTicket;


public class NegativeTicketTests extends BaseTicketTests {


    @Test(dataProvider = "negativeYourTicketDataProvider", dataProviderClass = ExternalDataProvider.class)

    public static void positiveYourTicket(String input, boolean expectedOutput) {
        List<Boolean> actualOutput = YourTicket.luckyTicket(input);
        Assertions.assertThat(actualOutput).isSubsetOf(expectedOutput);
    }

}
