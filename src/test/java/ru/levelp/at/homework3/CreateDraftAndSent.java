package ru.levelp.at.homework3;

import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class CreateDraftAndSent extends BaseClass {


    @Test(dataProvider = "test1")
    public void createDraftAndSent(String email, String title, String body) {
        //Если я не буду использовать шаг с рефрешем страницы,
        //то не получается выполнить шаг с кликом кнопки
        //появляется exception: org.openqa.selenium.StaleElementReferenceException:
        // stale element reference: element is not attached to the page document
        driver.findElement(By.xpath("//a[@href='/compose/']"));
        driver.navigate().refresh();
        driver.findElement(By.xpath("//a[@href='/compose/']")).click();
        driver.findElement(By.xpath("//label/div/div/input")).sendKeys(email);
        driver.findElement(By.xpath("//div/div/input[@name='Subject']")).sendKeys(title);
        driver.findElement(By.xpath("//div[@role='textbox']")).sendKeys(body);
        driver.findElement(By.xpath("//span[contains(text(),'Сохранить')]")).click();
        driver.findElement(By.xpath("//button[@title='Закрыть']")).click();
        driver.findElement(By.xpath(("//a[@href='/drafts/']"))).click();
        Assertions.assertThat(driver.findElement(By.xpath("//a[@href='/drafts/']")))
                .isEqualTo(driver.findElement(By.xpath("//a[@href='/drafts/']")));
        driver.findElement(By.xpath("//span[@title='" + email + "']"));
        driver.navigate().refresh();
        driver.findElement(By.xpath("//span[@title='" + email + "']")).click();
        Assertions.assertThat(driver.findElement(By.xpath("//input[@name='Subject']")))
                .isEqualTo(driver.findElement(By.xpath("//input[@name='Subject'][@value='" + title + "']")));
        driver.findElement(By.xpath("//div[normalize-space()='" + body + "']"));
        driver.findElement(By.xpath("//span[contains(text(),'Отправить')]")).click();
        driver.findElement(By.xpath("//span[@title='Закрыть']")).click();
        driver.findElement(By.xpath("//div[@class='octopus__icon octopus__icon_no-drafts']"));
        driver.findElement(By.xpath("//a[@title='Отправленные']")).click();
        driver.findElement(By.xpath("//span[@title='" + email + "']")).click();
        Assertions.assertThat(driver.findElement(By.xpath("//h2[normalize-space()='" + title + "']")))
                .isEqualTo(driver.findElement(By.xpath("//h2[normalize-space()='" + title + "']")));
    }


}




