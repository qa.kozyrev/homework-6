package ru.levelp.at.homework3;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class CreateEmailWithTestTitle extends BaseClass {


    @Test(dataProvider = "test2")
    public void createEmailWithTestTitle(String email, String title, String body) {

        driver.findElement(By.xpath("//a[@href='/compose/']"));
        driver.navigate().refresh();
        driver.findElement(By.xpath("//a[@href='/compose/']")).click();
        driver.findElement(By.xpath("//label/div/div/input")).sendKeys(email);
        driver.findElement(By.xpath("//div/div/input[@name='Subject']")).sendKeys(title);
        driver.findElement(By.xpath("//div[@role='textbox']")).sendKeys(body);
        driver.findElement(By.xpath("//span[contains(text(),'Отправить')]")).click();
        driver.findElement(By.xpath("//span[@title='Закрыть']")).click();
        driver.navigate().refresh();
        driver.findElement(By.xpath("//a[@title='Отправленные']")).click();
        driver.findElement(By.xpath(("//div/span[@title='" + email + "']"))).click();
        Assertions.assertThat(driver.findElement(By.xpath("//h2[contains(text(),'Self: " + title + "')]")))
                .isEqualTo(driver.findElement(By.xpath("//h2[contains(text(),'Self: " + title + "')]")));
        driver.findElement(By.xpath("//span[@title='" + email + "']"));

        //В случае, если в заголовке содержится слово "Тест", выполняем алгоритм в блоке if
        Pattern p = Pattern.compile(regexTest2);
        Matcher m = p.matcher(title);

        if (m.find()) {
            driver.navigate().refresh();
            driver.findElement(By.xpath("//a[@title='Все папки']")).click();
            WebElement element = new WebDriverWait(driver, Duration.ofSeconds(3))
                    .until(ExpectedConditions
                            .visibilityOfElementLocated(By.xpath("//div[@class='sidebar__full fn-enter']")));
            Actions action = new Actions(driver);
            action.moveToElement(element).perform();
            driver.findElement(By.xpath("//nav/a[@href='/1/'][@title='Тест']"));
            WebElement ele = driver.findElement(By.xpath("//nav/a[@href='/1/'][@title='Тест']"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", ele);
            driver.findElement(By.xpath("//span[@title='Иван Тестер <itester@internet.ru>']")).click();
            Assertions.assertThat(driver.findElement(By.xpath("//h2[contains(text(),'" + title + "')]")))
                    .isEqualTo(driver.findElement(By.xpath("//h2[contains(text(),'" + title + "')]")));
            driver.findElement(By.xpath("//span[@title='" + email + "']"));
        } else {
            System.out.println(driver.findElement(By.xpath("//h2[contains(text(),'Self: " + title + "')]")).getText());
        }

    }

}
