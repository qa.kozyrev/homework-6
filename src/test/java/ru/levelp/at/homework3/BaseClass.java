package ru.levelp.at.homework3;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.time.Duration;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;


public class BaseClass {

    protected static final String URL = "https://mail.ru";
    protected static WebDriver driver;
    public static final String email = "itester@internet.ru";
    public static final String password = "1qazXSW@12345";
    public static final String folderForTest2 = "Тест";
    public static final String regexTest2 = "\\bТест\\b";


    @DataProvider()
    public Object[][] test1() {
        return new Object[][]{
                {"qa.kozyrev@gmail.com", "Test", "TestBody1"},
        };
    }

    @DataProvider()
    public Object[][] test2() {
        return new Object[][]{
                {"itester@internet.ru", "Title", "TestBody1"},
                {"itester@internet.ru", "Тест", "TestBody3"}, //unicode %u0422%u0435%u0441%u0442
                {"itester@internet.ru", "Тест Заголовок", "TestBody3"}
        };
    }

    @DataProvider()
    public Object[][] test3() {
        return new Object[][]{
                {"itester@internet.ru", "Title", "TestBody1"},
                {"itester@internet.ru", "Title2", "TestBody3"},
        };
    }


    @BeforeMethod
    public void setUp() {
        //Log in
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to(URL);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(21));
        driver.findElement(By.cssSelector("[data-testid = 'enter-mail-primary']")).click();
        WebElement iframe = driver.findElement(By.cssSelector("div > iframe"));
        driver.switchTo().frame(iframe);
        driver.findElement(By.cssSelector("[name='username']")).sendKeys(email);
        driver.findElement(By.cssSelector("[data-test-id='next-button']")).click();
        driver.findElement(By.cssSelector("[name = 'password' ]")).sendKeys(password);
        driver.findElement(By.cssSelector("[data-test-id='submit-button']")).click();
        driver.switchTo().defaultContent();
        driver.findElement(By.cssSelector("[data-testid='whiteline-account']"));
        Assertions.assertThat(driver.findElements(By.cssSelector("[aria-label='" + email + "']")))
                .isEqualTo(driver.findElements(By.cssSelector("[aria-label='" + email + "']")));

    }


    @AfterMethod
    //Log out
    public void tearDown() {
        driver.findElement(By.cssSelector("[data-testid='whiteline-account']")).click();
        driver.findElement(By.xpath("//div[contains(text(),'Выйти')]")).click();
        driver.quit();
    }

}
