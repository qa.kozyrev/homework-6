package ru.levelp.at.homework3;

import java.time.Duration;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class CreateAndRemoveMessage extends BaseClass {

    @Test(dataProvider = "test3")
    public void createAndRemoveMessage(String email, String title, String body) {
        driver.findElement(By.xpath("//a[@href='/compose/']"));
        driver.navigate().refresh();
        driver.findElement(By.xpath("//a[@href='/compose/']")).click();
        driver.findElement(By.xpath("//label/div/div/input")).sendKeys(email);
        driver.findElement(By.xpath("//div/div/input[@name='Subject']")).sendKeys(title);
        driver.findElement(By.xpath("//div[@role='textbox']")).sendKeys(body);
        driver.findElement(By.xpath("//span[contains(text(),'Отправить')]")).click();
        driver.findElement(By.xpath("//span[@title='Закрыть']")).click();
        Assertions.assertThat(driver.findElement(By.xpath("//span[@title='Иван Тестер <itester@internet.ru>']")))
                .isEqualTo(driver.findElement(By.xpath("//span[@title='Иван Тестер <itester@internet.ru>']")));
        driver.navigate().refresh();
        driver.findElement(By.xpath("//span[@title='Иван Тестер <itester@internet.ru>']")).click();
        Assertions.assertThat(driver.findElement(By.xpath("//h2[contains(text(),'" + title + "')]")))
                .isEqualTo(driver.findElement(By.xpath("//h2[contains(text(),'" + title + "')]")));
        driver.findElement(By.xpath("//span[@title='" + email + "']"));
        driver.findElement(By.xpath("//span[@title='Удалить']")).click();
        WebElement element = new WebDriverWait(driver, Duration.ofSeconds(3))
                .until(ExpectedConditions
                        .visibilityOfElementLocated(By.xpath("//div[@id='sideBarContent']")));
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
        driver.findElement(By.xpath("//a[@href='/trash/']")).click();
        driver.findElement(By.xpath("//span[@title='Иван Тестер <itester@internet.ru>']")).click();
        Assertions.assertThat(driver.findElement(By.xpath("//h2[normalize-space()='" + title + "']")))
                .isEqualTo(driver.findElement(By.xpath("//h2[normalize-space()='" + title + "']")));


    }
}
