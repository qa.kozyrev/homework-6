package ru.levelp.at.homework6;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BaseClassPage {

    @FindBy(css = "[data-testid = 'enter-mail-primary']")

    private WebElement emailModal;

    @FindBy(css = "[name='username']")
    private WebElement login;

    @FindBy(css = "[data-test-id='next-button']")
    private WebElement nextButton;

    @FindBy(css = "[name = 'password' ]")
    private WebElement password;

    @FindBy(css = "[data-test-id='submit-button']")
    private WebElement submit;

    @FindBy(css = "div > iframe")
    private WebElement iframe;

    public void switchIframe() {
        WebDriverWait wait;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe));
    }

    public LoginPage(WebDriver driver) {
        super(driver);
        //this.driver = driver;
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(21));
        //PageFactory.initElements(driver, this);
    }

    public void openEmailModal() {
        this.emailModal.click();
    }

    public void enterEmail(String email) {
        this.login.sendKeys(email);
    }

    public void clickNextButtonInEmailModal() {
        this.nextButton.click();
    }

    public void enterPassword(String password) {
        this.password.sendKeys(password);
    }

    public void clickSubmitButtonInEmailModal() {
        this.submit.click();
    }
}
