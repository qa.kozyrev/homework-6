package ru.levelp.at.homework6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EmailPage extends BaseClassPage {
    /* public final WebDriver driver;
    public final WebDriverWait wait;*/

    @FindBy(xpath = "//a[@href='/compose/']")
    private WebElement emailCreationButton;

    @FindBy(xpath = "//label/div/div/input")
    private WebElement emailTextField;

    @FindBy(xpath = "//div/div/input[@name='Subject']")
    private WebElement titleTextField;

    @FindBy(xpath = "//div[@role='textbox']")
    private WebElement bodyTextField;

    @FindBy(xpath = "//span[contains(text(),'Сохранить')]")
    private WebElement emailSaveButton;

    @FindBy(xpath = "//button[@title='Закрыть']")
    private WebElement emailCloseButton;

    @FindBy(xpath = "//a[@title='Черновики']")
    private WebElement draftsFolderButton;

    @FindBy(xpath = "//button[@data-test-id='send']")
    private WebElement sentEmailButton;

    @FindBy(xpath = "//span[@title='Закрыть']")
    private WebElement closeInfoModalButton;

    @FindBy(xpath = "//span[@class='octopus__title']")
    private WebElement checkingDraftsFolderIsEmpty;

    @FindBy(xpath = "//a[@title='Отправленные']")

    private WebElement sentFolderIcon;

    @FindBy(css = "[aria-label='itester@inbox.ru']")
    private WebElement emailChecking;

    @FindBy(css = "[data-testid='whiteline-account']")
    private WebElement accountDataPlacement;

    @FindBy(xpath = "//div[contains(text(),'Выйти')]")
    private WebElement logOut;

    @FindBy(xpath = "(//span[@title='itester@internet.ru'])[1]")
    private WebElement emailItester;

    @FindBy(xpath = "//span[@title='qa.kozyrev@gmail.com']")
    private WebElement emailQaKozyrev;

    @FindBy(xpath = "//span[@title='Удалить']")
    private WebElement removeButton;

    @FindBy(xpath = "//div[@id='sideBarContent']")
    private WebElement sideBar;

    //a[@href='/trash/']
    @FindBy(xpath = "//a[@title= 'Корзина']")
    private WebElement trash;

    @FindBy(xpath = "//h2[normalize-space()='Иван Тестер <itester@internet.ru>']")
    private WebElement messageTitle;

    @FindBy(xpath = "//span[@title='Иван Тестер <itester@internet.ru>']")
    private WebElement checkingSentEmailTitleItester;

    @FindBy(xpath = "(//span[@title='qa.kozyrev@gmail.com'])[1]")
    private WebElement checkingSentEmailTitleQaKozyrev;

    @FindBy(xpath = "//div[normalize-space()='TestBody']")
    private WebElement testBody;

    @FindBy(xpath = "//a[@title='Все папки']")
    private WebElement allFolders;

    @FindBy(xpath = "//nav/a[@href='/1/'][@title='Тест']")
    private WebElement testFolder;

    @FindBy(xpath = "//span[@class='search-panel-button__text']")
    private WebElement searchField;

    @FindBy(xpath = "//div[contains(text(),'itester@internet.ru')]")
    private WebElement emailInSidePanel;

    //Assertions
    @FindBy(xpath = "//h2[normalize-space()='Title']")
    private WebElement getEmailTitle;

    @FindBy(xpath = "(//span[normalize-space()='Test'])[1]")
    private WebElement getTitle;

    public EmailPage(WebDriver driver) {
        super(driver);
        /*this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);*/
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public void createNewEmail() {
        wait.until(ExpectedConditions.visibilityOf(emailCreationButton)).click();
    }

    public void fillEmail(String email) {
        this.emailTextField.sendKeys(email);
    }

    public void fillTittle(String title) {
        this.titleTextField.sendKeys(title);
    }

    public void fillBody(String body) {
        this.bodyTextField.sendKeys(body);
    }

    public void saveEmail() {
        this.emailSaveButton.click();
    }

    public void closeEmailModal() {
        wait.until(ExpectedConditions.visibilityOf(emailCloseButton)).click();
    }

    public void openDraftsFolder() {
        this.draftsFolderButton.click();
    }

    public void sentEmail() {
        this.sentEmailButton.click();
    }

    public void closeInfoModal() {
        this.closeInfoModalButton.click();
    }

    public void checkDraftsFolderIsEmpty() {
        this.checkingDraftsFolderIsEmpty.click();
    }

    public void selectSentFolder() {
        this.sentFolderIcon.click();
    }

    public void clickAccountDataButton() {
        this.accountDataPlacement.click();
    }

    public void clickLogoutButton() {
        this.logOut.click();
    }

    public void selectMessageByEmailItester() {
        this.emailItester.click();
    }

    public void selectMessageByEmailQaKozyrev() {
        this.emailQaKozyrev.click();
    }

    public void clickRemoveButton() {
        this.removeButton.click();
    }

    public void openSideBar() {
        Actions action = new Actions(driver);
        /* Action mouseOverSideBar = action.moveToElement(sideBar).build();
        mouseOverSideBar.perform();*/
        action.moveToElement(sideBar).perform();
        wait.until(ExpectedConditions.visibilityOf(sideBar));
    }

    public void openTrashFolder() {
        Actions action = new Actions(driver);
        action.moveToElement(trash).build().perform();
        this.trash.click();
    }

    public void selectMessageByTitle() {
        this.messageTitle.click();
    }

    public void checkSentEmailTitleItester() {
        this.checkingSentEmailTitleItester.click();
    }

    public void checkSentEmailTitleQaKozyrev() {
        this.checkingSentEmailTitleQaKozyrev.click();
    }

    public String checkEmail() {
        return wait.until(ExpectedConditions.visibilityOf(emailChecking)).getText();
    }

    public void checkingEmailBody() {
        this.testBody.click();
    }

    public void selectAllFoldersFolder() {
        this.allFolders.click();
    }

    public String getEmailTitle() {
        return getEmailTitle.getText();
    }

    public String getEmailAddress() {
        return emailItester.getText();
    }

    public String getTitle() {
        return getTitle.getText();
    }

    public String getEmail() {
        return emailChecking.getText();
    }

    public String checkEmailInSidePanel() {
        return emailInSidePanel.getText();
    }
}
