package ru.levelp.at.homework6;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClassPage {

    public final WebDriver driver;
    public final WebDriverWait wait;

    public BaseClassPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(21));
        PageFactory.initElements(driver, this);
    }
}
